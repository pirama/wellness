   [].slice.call(document.querySelectorAll('button.progress-button')).forEach(function (bttn) {
    new ProgressButton(bttn, {
        callback: function (instance) {
            var progress = 0,
                interval = setInterval(function () {
                    progress = Math.min(progress + Math.random() * 0.1, 1);
                    instance._setProgress(progress);

                    if (progress === 1) {
                        instance._stop(1);
                        clearInterval(interval);
                    }
                }, 200);
        }
    });
});
//(function ($) {
//
//    $(function () {
//
//        var $window = $(window),
//            $body = $('body');
//
//        // Disable animations/transitions until the page has loaded.
//        $body.addClass('is-loading');
//
//        $window.on('load', function () {
//            window.setTimeout(function () {
//                $body.removeClass('is-loading');
//            }, 0);
//        });
//
//    });
//
//})(jQuery);


$(function () {

    $('section').scrollex({

        // Mode: Sets Scrollex to 'middle' mode (= midpoint between top/bottom edges must fall within contact area).
        mode: 'middle',

        // Initialize event: Add the 'inactive' class to this element as soon as Scrollex is initialized.
        initialize: function () {
            $(this).addClass('inactive');
        },

        // Enter event: Remove the 'inactive' class from this element.
        enter: function () {
            $(this).removeClass('inactive');
        },

        // Leave event: Apply the 'inactive' class to this element.
        leave: function () {
            $(this).addClass('inactive');
        }

    });

});

$(function () {

    $('header').scrollex({

        // Mode: Sets Scrollex to 'middle' mode (= midpoint between top/bottom edges must fall within contact area).
//        mode: 'top',
          // Initialize event: Add the 'inactive' class to this element as soon as Scrollex is initialized.
        initialize: function () {
            $(this).addClass('inactive');
        },

        // Enter event: Remove the 'inactive' class from this element.
        enter: function () {
            $(this).removeClass('inactive');
        },

        // Leave event: Apply the 'inactive' class to this element.
        leave: function () {
            $(this).addClass('inactive');
        }

    });

});
jQuery(document).ready(function ($) {
    //set your google maps parameters
    var latitude = 38.765224,
        longitude = -121.270998,
        map_zoom = 14;

    //google map custom marker icon - .png fallback for IE11
    var is_internetExplorer11 = navigator.userAgent.toLowerCase().indexOf('trident') > -1;
    var marker_url = (is_internetExplorer11) ? 'img/cd-icon-location.png' : 'img/cd-icon-location.svg';

    //define the basic color of your map, plus a value for saturation and brightness
    var main_color = '#2d313f',
        saturation_value = -20,
        brightness_value = 5;

    //we define here the style of the map
    var style = [
        {
            //set saturation for the labels on the map
            elementType: "labels",
            stylers: [
                {
                    saturation: saturation_value
                }
   ]
  },
        { //poi stands for point of interest - don't show these lables on the map 
            featureType: "poi",
            elementType: "labels",
            stylers: [
                {
                    visibility: "off"
                }
   ]
  },
        {
            //don't show highways lables on the map
            featureType: 'road.highway',
            elementType: 'labels',
            stylers: [
                {
                    visibility: "off"
                }
         ]
     },
        {
            //don't show local road lables on the map
            featureType: "road.local",
            elementType: "labels.icon",
            stylers: [
                {
                    visibility: "off"
                }
   ]
  },
        {
            //don't show arterial road lables on the map
            featureType: "road.arterial",
            elementType: "labels.icon",
            stylers: [
                {
                    visibility: "off"
                }
   ]
  },
        {
            //don't show road lables on the map
            featureType: "road",
            elementType: "geometry.stroke",
            stylers: [
                {
                    visibility: "off"
                }
   ]
  },
  //style different elements on the map
        {
            featureType: "transit",
            elementType: "geometry.fill",
            stylers: [
                {
                    hue: main_color
                },
                {
                    visibility: "on"
                },
                {
                    lightness: brightness_value
                },
                {
                    saturation: saturation_value
                }
   ]
  },
        {
            featureType: "poi",
            elementType: "geometry.fill",
            stylers: [
                {
                    hue: main_color
                },
                {
                    visibility: "on"
                },
                {
                    lightness: brightness_value
                },
                {
                    saturation: saturation_value
                }
   ]
  },
        {
            featureType: "poi.government",
            elementType: "geometry.fill",
            stylers: [
                {
                    hue: main_color
                },
                {
                    visibility: "on"
                },
                {
                    lightness: brightness_value
                },
                {
                    saturation: saturation_value
                }
   ]
  },
        {
            featureType: "poi.sport_complex",
            elementType: "geometry.fill",
            stylers: [
                {
                    hue: main_color
                },
                {
                    visibility: "on"
                },
                {
                    lightness: brightness_value
                },
                {
                    saturation: saturation_value
                }
   ]
  },
        {
            featureType: "poi.attraction",
            elementType: "geometry.fill",
            stylers: [
                {
                    hue: main_color
                },
                {
                    visibility: "on"
                },
                {
                    lightness: brightness_value
                },
                {
                    saturation: saturation_value
                }
   ]
  },
        {
            featureType: "poi.business",
            elementType: "geometry.fill",
            stylers: [
                {
                    hue: main_color
                },
                {
                    visibility: "on"
                },
                {
                    lightness: brightness_value
                },
                {
                    saturation: saturation_value
                }
   ]
  },
        {
            featureType: "transit",
            elementType: "geometry.fill",
            stylers: [
                {
                    hue: main_color
                },
                {
                    visibility: "on"
                },
                {
                    lightness: brightness_value
                },
                {
                    saturation: saturation_value
                }
   ]
  },
        {
            featureType: "transit.station",
            elementType: "geometry.fill",
            stylers: [
                {
                    hue: main_color
                },
                {
                    visibility: "on"
                },
                {
                    lightness: brightness_value
                },
                {
                    saturation: saturation_value
                }
   ]
  },
        {
            featureType: "landscape",
            stylers: [
                {
                    hue: main_color
                },
                {
                    visibility: "on"
                },
                {
                    lightness: brightness_value
                },
                {
                    saturation: saturation_value
                }
   ]

  },
        {
            featureType: "road",
            elementType: "geometry.fill",
            stylers: [
                {
                    hue: main_color
                },
                {
                    visibility: "on"
                },
                {
                    lightness: brightness_value
                },
                {
                    saturation: saturation_value
                }
   ]
  },
        {
            featureType: "road.highway",
            elementType: "geometry.fill",
            stylers: [
                {
                    hue: main_color
                },
                {
                    visibility: "on"
                },
                {
                    lightness: brightness_value
                },
                {
                    saturation: saturation_value
                }
   ]
  },
        {
            featureType: "water",
            elementType: "geometry",
            stylers: [
                {
                    hue: main_color
                },
                {
                    visibility: "on"
                },
                {
                    lightness: brightness_value
                },
                {
                    saturation: saturation_value
                }
   ]
  }
 ];

    //set google map options
    var map_options = {
            center: new google.maps.LatLng(latitude, longitude),
            zoom: map_zoom,
            panControl: false,
            zoomControl: false,
            mapTypeControl: false,
            streetViewControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false,
            styles: style,
        }
        //inizialize the map
    var map = new google.maps.Map(document.getElementById('google-container'), map_options);
    //add a custom marker to the map				
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(latitude, longitude),
        map: map,
        visible: true,
        icon: marker_url,
    });

    //add custom buttons for the zoom-in/zoom-out on the map
    function CustomZoomControl(controlDiv, map) {
        //grap the zoom elements from the DOM and insert them in the map 
        var controlUIzoomIn = document.getElementById('cd-zoom-in'),
            controlUIzoomOut = document.getElementById('cd-zoom-out');
        controlDiv.appendChild(controlUIzoomIn);
        controlDiv.appendChild(controlUIzoomOut);

        // Setup the click event listeners and zoom-in or out according to the clicked element
        google.maps.event.addDomListener(controlUIzoomIn, 'click', function () {
            map.setZoom(map.getZoom() + 1)
        });
        google.maps.event.addDomListener(controlUIzoomOut, 'click', function () {
            map.setZoom(map.getZoom() - 1)
        });
    }

    var zoomControlDiv = document.createElement('div');
    var zoomControl = new CustomZoomControl(zoomControlDiv, map);

    //insert the zoom div on the top left of the map
    map.controls[google.maps.ControlPosition.LEFT_TOP].push(zoomControlDiv);
});

$(function () {
    $('form').submit(function () {
        return false;
    });
});